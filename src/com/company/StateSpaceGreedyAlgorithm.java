package com.company;

import java.util.ArrayList;
import java.util.Random;

public class StateSpaceGreedyAlgorithm {
    private int [][]citiesMatrix;
    private  int [][]costMatrix;
    protected int counterNumberOfDFSCalling = 0;
    int currentCost = 0;
    int bestCost = 2147483647;
    int previousCost = 0;
    int []currentPath = new int[10];
    int [] bestPath = new int[10];
    private int iterator = 0;
    ArrayList<Integer> citiesId;
    // Getters

    StateSpaceGreedyAlgorithm (int nrOfCities, boolean random)
    {
        citiesId = new ArrayList<Integer>(nrOfCities);
        for (int i = 0; i < nrOfCities; i++)
        	citiesId.add(i);
        if (random)
        	generateRandomCitiesMatrix(nrOfCities);
        else
        	fillCitiesMatrix();
        calculateCostMatrix();
    }
    
    protected int[][] getCitiesMatrix()
    {
        return citiesMatrix;
    }
    
    void DFS (Integer newCurrentNode) {
    	DFS(newCurrentNode, citiesId);
    }
    
    void DFS(Integer newCurrentNode, ArrayList<Integer>toVisit)
    {
        ++counterNumberOfDFSCalling;
        currentPath[iterator++] = newCurrentNode;
    	toVisit.removeIf( p-> p.equals(newCurrentNode));
    	System.out.println("Current node: "  + newCurrentNode + " Number of node to visit : " + toVisit.size());
    	if(toVisit.size() == 0)
    	{
            currentCost += costMatrix[newCurrentNode][0];
            currentPath[iterator++] = 0;
            System.out.print(" I found a new path ! ");
            System.out.println(" Path cost : " + currentCost);
            if (currentCost < bestCost)
            {
                bestCost = currentCost;
                bestPath = currentPath.clone();
                System.out.println(" ==== This path is better than the old one !!!");
            }
            currentCost -= costMatrix[newCurrentNode][0];
            iterator--;
            return;
        }
    	else
        {
    		Integer []nodesArray = new Integer[toVisit.size()];
    		toVisit.toArray(nodesArray);
    		int nodes = nodesArray.length;
    		int bestLocalCandidate = 0;
    		int firstLocalCandidate = 0;
    		int secondLocalCandidate = 0;
    		int temp;
    		do
    		{
	            for (int i = 0; i < nodesArray.length-1; i++)
	            {
	            	firstLocalCandidate = nodesArray[i];
	            	secondLocalCandidate = nodesArray[i+1];
	                
	                if (costMatrix[newCurrentNode][firstLocalCandidate] > costMatrix[newCurrentNode][secondLocalCandidate])
	                {
	                	temp = firstLocalCandidate;
	                	nodesArray[i+1] = temp;
	                	nodesArray[i] = secondLocalCandidate;
	                }
	            }
	            nodes--;
    		} while (nodes > 0);
//    		for (int i: nodesArray)
//    			System.out.print(i);
    		bestLocalCandidate = nodesArray[0];
    		System.out.println("Current cost: " + currentCost + " Next node to visit " + bestLocalCandidate);
            currentCost += costMatrix[newCurrentNode][bestLocalCandidate];
        
            DFS(bestLocalCandidate, new ArrayList<>(toVisit));
            currentCost -= costMatrix[newCurrentNode][bestLocalCandidate];
            iterator--;
        }
    }
    
    
    // Functional methods
    protected void calculateCostMatrix()
    {
        costMatrix = new int[citiesMatrix.length][citiesMatrix.length];
        for (int i = 0 ; i < costMatrix.length ; i++)
        {
            costMatrix[i][i] = 0;
        }
        for (int i = 0 ; i < costMatrix.length ; i++)
        {
           for (int j = 0 ; j < costMatrix[i].length ; j++)
           {
               //System.out.println(i + "<-i  j->" + j );
               //System.out.print((Math.pow((citiesMatrix[j][1] - citiesMatrix[i][1]),2)));

               costMatrix[i][j] = (int) Math.sqrt( ( ( ( Math.pow((citiesMatrix[j][1] - citiesMatrix[i][1]), 2) + Math.pow((citiesMatrix[j][2] - citiesMatrix[i][2]), 2 ) ) ) ) );
               //System.out.println( (citiesMatrix[j][1] + " - " + citiesMatrix[i][1]) + " + " +  citiesMatrix[j][2] + " - " + citiesMatrix[i][2] );
               //System.out.println( (citiesMatrix[j][1] - citiesMatrix[i][1]) + " + " +  (citiesMatrix[j][2] - citiesMatrix[i][2]) );
               System.out.print( " " + costMatrix[i][j]);
           }
           System.out.println();
        }
    }


    protected void fillCitiesMatrix()
    {
        citiesMatrix = new int[][]
                {{0, /*x*/ 2,  /*y*/  2},
                {1,  /*x*/ -2, /*y*/  4},
                {2,  /*x*/  1, /*y*/ -2},
                {3,  /*x*/ -4, /*y*/ -1},
                {4,  /*x*/ 0, /*y*/ 0}};
//        citiesMatrix = new int[][]
//                {{0, /*x*/ 2,  /*y*/  2},
//                        {1,  /*x*/ -2, /*y*/  4},
//                        {2,  /*x*/  1, /*y*/ -2},
//                        {3,  /*x*/ -4, /*y*/ -1}};
    }

    protected void generateRandomCitiesMatrix(int numberOfCities)
    {
        generateRandomCitiesMatrix(numberOfCities, false, 10);
    }

    protected void generateRandomCitiesMatrix(int numberOfCities, boolean print, int randomRange)
    {
        Random randomGridReference = new Random();
        citiesMatrix = new int[numberOfCities][3];
        for (int i = 0; i < numberOfCities; i++)
            for (int j = 1; j < 3; j++)
                citiesMatrix[i][j] = randomGridReference.nextInt(randomRange);
        System.out.println("Generated! (Use overloaded method in case of other random range, printing content)");
        currentPath = new int[numberOfCities+1];
        bestPath = new int[numberOfCities+1];
        if(print)
        {
            for (int i = 0; i < numberOfCities; i++) {
                for (int j = 0; j < 3; j++)
                    System.out.print(" " + citiesMatrix[i][j]);
                System.out.println();
            }
        }
    }
}
