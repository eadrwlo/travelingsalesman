package com.company;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        
    	final int CITIES_NUMBER = 15;
        
//        StateSpace one = new StateSpace(CITIES_NUMBER, true);
//        one.DFS(0);
        
        StateSpaceGreedyAlgorithm one1 = new StateSpaceGreedyAlgorithm(CITIES_NUMBER, true);
        one1.DFS(0);
        
        
//        System.out.println("Counter: " + one.counterNumberOfDFSCalling +
//                            " BestCost " + one.bestCost );
//
//        System.out.println("Best path: " );
//        for (int a: one.bestPath)
//        {
//            System.out.print(a);
//        }
    }
}
